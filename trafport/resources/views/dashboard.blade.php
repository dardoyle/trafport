@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard concessionnaire')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="card">
            <div class="card-header card-header-tabs card-header-primary">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <span class="nav-tabs-title"></span>
                  <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                      <div class="nav-link active">
                        <i class="material-icons">import_contacts</i>&nbsp _ _ &nbsp Relevés &nbsp _ _ &nbsp
                        <div class="ripple-container"></div>
                      </div>
                    </li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane active" id="profile">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td></td>
                        <td><button type="button" class="btn btn-primary btn-sm" rel="tooltip" title="Encoder">Encoder</button></td>
                        <td class="td-actions text-right">
                          <button type="button" rel="tooltip" title="Encoder" class="btn btn-primary btn-link btn-sm">
                            <i class="material-icons">app_registration</i>
                          </button>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><button type="button" class="btn btn-primary btn-sm" rel="tooltip" title="Modifier">Modifier</button></td>
                        <td class="td-actions text-right">
                          <button type="button" rel="tooltip" title="Modifier" class="btn btn-primary btn-link btn-sm">
                            <i class="material-icons">edit</i>
                          </button>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><button type="button" class="btn btn-primary btn-sm" rel="tooltip" title="Consulter">Consulter</button></td>
                        <td class="td-actions text-right">
                          <button type="button" rel="tooltip" title="Consul" class="btn btn-primary btn-link btn-sm">
                            <i class="material-icons">checklist</i>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="card">
            <div class="card-header card-header-warning">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <span class="nav-tabs-title"></span>
                  <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                      <div class="nav-link active">
                        <i class="material-icons">import_contacts</i>&nbsp _ _ &nbsp Profil &nbsp _ _ &nbsp
                        <div class="ripple-container"></div>
                      </div>
                    </li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="card-body table-responsive">
              <table class="table table-hover">
                <thead class="text-warning">
                  <th>ID</th>
                  <th>Name</th>
                  <th>Salary</th>
                  <th>Country</th>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Dakota Rice</td>
                    <td>$36,738</td>
                    <td>Niger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Minerva Hooper</td>
                    <td>$23,789</td>
                    <td>Curaçao</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Sage Rodriguez</td>
                    <td>$56,142</td>
                    <td>Netherlands</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Philip Chaney</td>
                    <td>$38,735</td>
                    <td>Korea, South</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });
  </script>
@endpush
